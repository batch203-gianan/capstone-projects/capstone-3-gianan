import Carousel from 'react-bootstrap/Carousel';
import { Container } from "react-bootstrap";

export default function ShowCarousel() {

  return (
    <Container className="car mb-5">

    <Carousel fade>
          <Carousel.Item>
            <img
              className="d-block align-center mx-auto h-100 w-100 rounded"
              src="https://files01.pna.gov.ph/ograph/2022/01/28/sabongcavite-website-photo-1.jpg"
              alt="Second slide"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block align-center mx-auto h-100 w-100 rounded"
              src="https://tribune.net.ph/wp-content/uploads/2022/08/Untitled-1-copy-364.jpg"
              alt="Second slide"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block align-center mx-auto h-100 w-100 rounded"
              src="https://dailyguardian.com.ph/wp-content/uploads/2021/02/cockfighting.jpg"
              alt="Second slide"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block align-center mx-auto h-100 w-100 rounded"
              src="https://sa.kapamilya.com/absnews/abscbnnews/media/2020/afp/06/05/20200605-cockfighting-san-roque-afp-10087770.jpg"
              alt="Second slide"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block align-center mx-auto h-100 w-100 rounded"
              src="https://www.wheninmanila.com/wp-content/uploads/2015/02/VICE-Sabong.jpg"
              alt="Second slide"
            />
          </Carousel.Item>

        </Carousel>

    </Container>


  );
}

