import Banner from "../components/Banner";
import ShowCarousel from "../components/ShowCarousel"

export default function Home() {
  const data = {
    title: "Sabong Depot",
    content: "Your one-stop shop for your fighting cocks!",
    destination: "/products",
    label: "Shop Now!",
  };

  return (
    <>
      <Banner data={data} />
      <ShowCarousel />
    </>
  );
}
